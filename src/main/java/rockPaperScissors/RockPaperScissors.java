package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();

    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");

    public void run() {
        while (true) {
            System.out.println("Let's play round "+roundCounter);
            String humanChoice = readInput("Your choice (Rock/Paper/Scissors)?");
            String computerChoice = computerChoice();

            if (humanChoice.equals(computerChoice)) {
                System.out.println("Human chose "+humanChoice+", computer chose "+computerChoice+". It's a tie!");
                roundCounter++;
                System.out.println("Score: human "+humanScore+", computer "+computerScore);

            }
            else if (humanChoice.equals("rock")) {
                if (computerChoice.equals("scissors")) {
                    System.out.println("Human chose "+humanChoice+", computer chose "+computerChoice+". Human wins!");
                    humanScore++;
                    roundCounter++;
                    System.out.println("Score: human "+humanScore+", computer "+computerScore);
                }
                else if (computerChoice.equals("paper")) {
                    System.out.println("Human chose "+humanChoice+", computer chose "+computerChoice+". Computer wins!");
                    computerScore++;
                    roundCounter++;
                    System.out.println("Score: human "+humanScore+", computer "+computerScore);
                }
            }
            else if (humanChoice.equals("scissors")) {
                if (computerChoice.equals("paper")) {
                    System.out.println("Human chose "+humanChoice+", computer chose "+computerChoice+". Human wins!");
                    humanScore++;
                    roundCounter++;
                    System.out.println("Score: human "+humanScore+", computer "+computerScore);
                }
                else if (computerChoice.equals("rock")) {
                    System.out.println("Human chose "+humanChoice+", computer chose "+computerChoice+". Computer wins!");
                    computerScore++;
                    roundCounter++;
                    System.out.println("Score: human "+humanScore+", computer "+computerScore);
                }
            }
            else if (humanChoice.equals("paper")) {
                if (computerChoice.equals("scissors")) {
                    System.out.println("Human chose "+humanChoice+", computer chose "+computerChoice+". Computer wins!");
                    computerScore++;
                    roundCounter++;
                    System.out.println("Score: human "+humanScore+", computer "+computerScore);
                }
                else if (computerChoice.equals("rock")) {
                    System.out.println("Human chose "+humanChoice+", computer chose "+computerChoice+". Human wins!");
                    humanScore++;
                    roundCounter++;
                    System.out.println("Score: human "+humanScore+", computer "+computerScore);
                }
            }
            else {
                System.out.println("I do not understand "+humanChoice+". Could you try again?");
                continue;
            }
            String continueGame = readInput("Do you wish to continue playing? (y/n)?");
            if (continueGame.equals("y")) {
                continue;
            }
            else {
                System.out.println("Bye bye :)");
                break;
            }
        }
    }

    public String computerChoice() {
        Random obj=new Random();
        int move=obj.nextInt(3);
        String choice = rpsChoices.get(move);
        return choice;
    }



    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
